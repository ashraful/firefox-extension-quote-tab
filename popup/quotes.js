const _QUOTE_DB = {};

_QUOTE_DB._LIST = [
  {
  	"quote": "Admit nothing, deny everything, make counter-accusations",
  	"author": "Roger J. Stone Jr."
  },
  {
  	"quote": "Nothing is constant in this world except [censored] front-end issues, that is the main stability source on our Earth",
  	"author": "0x44617665"
  },
  {
    "quote": "Let your plans be dark and impenetrable as night, and when you move, fall like a thunderbolt.",
    "author": "Sun Tzu"
  },
  {
    "quote": "Buying books would be a good thing if one could also buy the time to read them.",
    "author": "Arthur Schopenhauer"
  },
  {
    "quote": "All the lives I could live, all the people I will never know, never will be, they are everywhere. That is all that the world is.",
    "author": "Aleksandar Hemon"
  },
  {
    "quote": "Bureaucracy is a construction by which a person is conveniently separated from the consequences of his or her actions.",
    "author": "Nassim Taleb"
  },
  {
    "quote": "Facts do not cease to exist because they are ignored.",
    "author": "Aldous Huxley"
  },
  {
    "quote": "I am certainly convinced that it is one of the greatest impulses of mankind to arrive at something higher than a natural state.",
    "author": "James Baldwin"
  },
  {
    "quote": "We must take care not to admit as true anything which is only probable. For when one falsity has been let in, infinite others follow.",
    "author": "Baruch Spinoza"
  },
  {
    "quote": "The price of anything is the amount of life you exchange for it.",
    "author": "Henry David Thoreau"
  },
  {
    "quote": "Evil destroys even itself.",
    "author": "Aristotle"
  },
  {
    "quote": "The pursuit of philosophy is founded on the belief that knowledge is good, even if what is known is painful.",
    "author": "Bertrand Russell"
  },
  {
    "quote": "The beauty of things must be that they end.",
    "author": "Jack Kerouac"
  },
  {
    "quote": "I'm a slave to my emotions, to my likes, to my hatred of boredom, to most of my desires.",
    "author": "F. Scott Fitzgerald"
  },
  {
    "quote": "Never stand begging for that which you have the power to earn.",
    "author": "Miguel de Cervantes"
  },
  {
    "quote": "When it was too late for rescue, it was still early enough for revenge.",
    "author": "Kazuo Ishiguro"
  },
  {
    "quote": "I can barely conceive of a type of beauty in which there is no Melancholy.",
    "author": "Charles Baudelaire"
  },
  {
    "quote": "There's always, there'll always be, more story. That's what story is.",
    "author": "Ali Smith"
  },
  {
    "quote": "History's chief use is only to discover of the constant and universal principles of human nature. ",
    "author": "David Hume"
  },
  {
    "quote": "Nature has planted in our minds an insatiable longing to see the truth.",
    "author": "Cicero"
  },
  {
    "quote": "The beauty of a living thing is not the atoms that go into it, but the way those atoms are put together.",
    "author": "Carl Sagan"
  },
  {
    "quote": "Respond to every call that excites your spirit.",
    "author": "Jalāl Rumi"
  },
  {
    "quote": "The vast majority of human beings dislike and even actually dread all notions with which they are not familiar ... Hence it comes about that at their first appearance innovators have generally been persecuted, and always derided as fools and madmen.",
    "author": "Aldous Huxley"
  },
  {
    "quote": "The state calls its own violence law, but that of the individual crime.",
    "author": "Max Stirner"
  },
  {
    "quote": "You can never cross the ocean unless you have the courage to lose sight of the shore.",
    "author": "Andrè Gide"
  },
  {
    "quote": "A mind that is stretched by new experiences can never go back to its old dimensions.",
    "author": "Oliver Wendell Holmes, Jr."
  },
  {
    "quote": "Come let us be friends for once. Let us make life easy on us. Let us be loved ones and lovers. The earth shall be left to no one.",
    "author": "Yunus Emre"
  },
  {
    "quote": "Some people never go crazy. What truly horrible lives they must lead.",
    "author": "Charles Bukowski"
  },
  {
    "quote": "The infidels of one age have been the aureoled saints of the next. The destroyers of the old are the creators of the new.",
    "author": "Robert G. Ingersoll"
  },
  {
    "quote": "There are no beautiful surfaces without a terrible depth.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "There is a strange consuming happiness in acting with the full knowledge that whatever one is doing may very well be one's last act on earth.",
    "author": "Carlos Castaneda"
  },
  {
    "quote": "Everything considered, a determined soul will always manage.",
    "author": "Albert Camus"
  },
  {
    "quote": "Facts do not cease to exist because they are ignored.",
    "author": "Aldous Huxley"
  },
  {
    "quote": "Self-control is the weakest of all things in one whose mind is not possessed by wisdom.",
    "author": "Aeschylus"
  },
  {
    "quote": "There is hardly a better way to avoid discussion than by releasing an argument from the control of the present and by saying that only the future will reveal its merits.",
    "author": "Hannah Arendt"
  },
  {
    "quote": "When people begin to philosophize they seem to think it necessary to make themselves artificially stupid.",
    "author": "Bertrand Russell"
  },
  {
    "quote": "Buying books would be a good thing if one could also buy the time to read them.",
    "author": "Arthur Schopenhauer"
  },
  {
    "quote": "When the people are being beaten with a stick, they are not much happier if it is called the People's Stick.",
    "author": "Mikhail Bakunin"
  },
  {
    "quote": "We judge of man's wisdom by his hope.",
    "author": "Ralph Waldo Emerson"
  },
  {
    "quote": "I wonder if the artist ever lives his life – he is so busy recreating it. To create is to live. Only as I write do I realize myself. I don't know what that does to life.",
    "author": "Anne Sexton"
  },
  {
    "quote": "There's something to be said for hunger: at least it lets you know you're still alive.",
    "author": "Margaret Atwood"
  },
  {
    "quote": "Conquer yourself rather than the world.",
    "author": "Rene Descartes"
  },
  {
    "quote": "When you resort to violence to prove a point, you've just experienced a profound failure of imagination.",
    "author": "Sherman Alexie"
  },
  {
    "quote": "What man of us has never felt, walking through the twilight or writing down a date from his past, that he has lost something infinite?",
    "author": "Jorge Luis Borges"
  },
  {
    "quote": "Recompense injury with justice, and recompense kindness with kindness.",
    "author": "Confucius"
  },
  {
    "quote": "If everything on earth were rational, nothing would ever happen.",
    "author": "Fyodor Dostoyevsky"
  },
  {
    "quote": "No man was ever wise by chance.",
    "author": "Seneca"
  },
  {
    "quote": "Self-pity is a sin. It is a form of living suicide.",
    "author": "Charles J. Shields"
  },
  {
    "quote": "I love the rain. I love how it softens the outlines of things. The world becomes softly blurred, and I feel like I melt right into it.",
    "author": "Hanamoto Hagumi"
  },
  {
    "quote": "Life's what you see in people's eyes; life's what they learn, and, having learnt it, never, though they seek to hide it, cease to be aware of.",
    "author": "Virginia Woolf"
  },
  {
    "quote": "Of all tyrannies a tyranny sincerely exercised for the good of its victims may be the most oppressive.",
    "author": "C. S. Lewis"
  },
  {
    "quote": "There is no genius without a mixture of madness.",
    "author": "Seneca"
  },
  {
    "quote": "In civil life we can scarcely meet a single person who does not complain of his existence; many even throw away as much of it as they can.",
    "author": "Jean-Jacques Rousseau"
  },
  {
    "quote": "The goal of life is to make your heartbeat match the beat of the Universe, to match your nature with Nature.",
    "author": "Joseph Campbell"
  },
  {
    "quote": "First we feel. Then we fall.",
    "author": "James Joyce"
  },
  {
    "quote": "The wisest man preaches no doctrines; he has no scheme; he sees no rafter, not even a cobweb, against the heavens. It is clear sky.",
    "author": "Henry David Thoreau"
  },
  {
    "quote": "When you have eliminated the impossible, whatever remains, however improbable, must be the truth.",
    "author": "Arthur Conan Doyle"
  },
  {
    "quote": "Raise your words, not voice. It is rain that grows flowers, not thunder.",
    "author": "Jalāl Rumi"
  },
  {
    "quote": "To be yourself in a world that is constantly trying to make you something else is the greatest accomplishment.",
    "author": "Ralph Waldo Emerson"
  },
  {
    "quote": "A harmless man is not a good man. A good man is a very, very dangerous man who has it under voluntary control.",
    "author": "Jordan Peterson"
  },
  {
    "quote": "Waste no more time arguing what a good person should be. Be one.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "My own opinion is enough for me and I claim the right to have it defended against any consensus, any majority.",
    "author": "Christopher Hitchens"
  },
  {
    "quote": "Nothing so promotes the growth of consciousness as the confrontation of opposites.",
    "author": "Carl Jung"
  },
  {
    "quote": "Therefore, find your own way, open your treasure house, invent your own answer to the chaos.",
    "author": "Gertrude Stein"
  },
  {
    "quote": "Men are born ignorant, not stupid; they are made stupid by education.",
    "author": "Bertrand Russell"
  },
  {
    "quote": "The art of progress is to preserve order amid change, and to preserve change amid order.",
    "author": "Alfred North Whitehead"
  },
  {
    "quote": "Just as there is no loss of basic energy in the universe, so no thought or action is without its effects, present or ultimate, seen or unseen, felt or unfelt.",
    "author": "Norman Cousins"
  },
  {
    "quote": "The thought of suicide is a powerful solace: by means of it one gets through many a bad night.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "Let us eat and drink, because tomorrow we shall die — but this is sensuality's cowardly lust for life, that contemptible order of things where one lives in order to eat and drink, instead of eating and drinking in order to live.",
    "author": "Søren Kierkegaard"
  },
  {
    "quote": "Some wounds cut us so deep that they stop us. Stop us from letting go, from growing up, from seeing the truth.",
    "author": "Laurell K. Hamilton"
  },
  {
    "quote": "All things come into being by a conflict of opposites.",
    "author": "Heraclitus"
  },
  {
    "quote": "What's coming is as to nothing what has been.",
    "author": "Terence McKenna"
  },
  {
    "quote": "People nowadays think that scientists exist to instruct them, poets, musicians, etc. to give them pleasure. The idea that these have something to teach them — that does not occur to them.",
    "author": "Ludwig Wittgenstein"
  },
  {
    "quote": "While I am writing, I'm far away; and when I come back, I've gone.",
    "author": "Pablo Neruda"
  },
  {
    "quote": "I've reached the age where bruises are formed from failures within rather than accidents without.",
    "author": "Nicole Krauss"
  },
  {
    "quote": "Verily you are suspended like scales between your sorrow and your joy. Only when you are empty are you at a standstill and balanced.",
    "author": "Kahlil Gibran"
  },
  {
    "quote": "Bureaucracy is a construction by which a person is conveniently separated from the consequences of his or her actions.",
    "author": "Nassim Taleb"
  },
  {
    "quote": "I was like a patient who cannot tell the doctor where it hurts, only that it does.",
    "author": "Khaled Hosseini"
  },
  {
    "quote": "I burn, consumed; and resurrect, half-slain.",
    "author": "Vita Sackville-West"
  },
  {
    "quote": "Just as the constant increase of entropy is the basic law of the universe, so it is the basic law of life to be ever more highly structured and to struggle against entropy.",
    "author": "Vaclav Havel"
  },
  {
    "quote": "The shadow disguises itself in our projections, when we react intensely to a trait in others that we fail to see in ourselves.",
    "author": "Connie Zweig"
  },
  {
    "quote": "The gem cannot be polished without friction, nor man perfected without trials.",
    "author": "Confucius"
  },
  {
    "quote": "The least movement is of importance to all nature. The entire ocean is affected by a pebble.",
    "author": "Blaise Pascal"
  },
  {
    "quote": "Until we have met the monsters in ourselves, we keep trying to slay them in the outer world. And we find that we cannot. For all darkness in the world stems from darkness in the heart. And it is there that we must do our work.",
    "author": "Marianne Williamson"
  },
  {
    "quote": "But man is not made for defeat. A man can be destroyed but not defeated.",
    "author": "Ernest Hemingway"
  },
  {
    "quote": "To deny my younger self would have been to deny my present self's validity.",
    "author": "Edward Snowden"
  },
  {
    "quote": "Good judgment comes from experience, and a lot of that comes from bad judgment.",
    "author": "Will Rogers"
  },
  {
    "quote": "Learning never exhausts the mind.",
    "author": "Leonardo da Vinci"
  },
  {
    "quote": "Permanence, perseverance and persistence in spite of all obstacles, discouragements, and impossibilities: It is this, that in all things distinguishes the strong soul from the weak.",
    "author": "Thomas Carlyle"
  },
  {
    "quote": "The supreme art of war is to subdue the enemy without fighting.",
    "author": "Sun Tzu"
  },
  {
    "quote": "If you cannot do great things, do small things in a great way.",
    "author": "Napoleon Hill"
  },
  {
    "quote": "There is only one corner of the universe you can be certain of improving, and that's your own self.",
    "author": "Aldous Huxley"
  },
  {
    "quote": "Keep your face always toward the sunshine — and shadows will fall behind you.",
    "author": "Walt Whitman"
  },
  {
    "quote": "A leader is one who knows the way, goes the way, and shows the way.",
    "author": "John C. Maxwell"
  },
  {
    "quote": "Very little is needed to make a happy life; it is all within yourself, in your way of thinking.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "If opportunity doesn't knock, build a door.",
    "author": "Milton Berle"
  },
  {
    "quote": "Let us be grateful to people who make us happy, they are the charming gardeners who make our souls blossom.",
    "author": "Marcel Proust"
  },
  {
    "quote": "Wise men speak because they have something to say; Fools because they have to say something.",
    "author": "Plato"
  },
  {
    "quote": "When we are no longer able to change a situation — we are challenged to change ourselves.",
    "author": "Viktor E. Frankl"
  },
  {
    "quote": "Blessed are the hearts that can bend; they shall never be broken.",
    "author": "Albert Camus"
  },
  {
    "quote": "The only thing necessary for the triumph of evil is for good men to do nothing.",
    "author": "Edmund Burke"
  },
  {
    "quote": "In all chaos there is a cosmos, in all disorder a secret order.",
    "author": "Carl Jung"
  },
  {
    "quote": "I think in terms of the day's resolutions, not the years'.",
    "author": "Henry Moore"
  },
  {
    "quote": "You are so much stronger than the world has ever believed you could be. The world is waiting for you to set it on fire. Trust in yourself and burn.",
    "author": "Clementine von Radics"
  },
  {
    "quote": "A man's work is nothing but this slow trek to rediscover, through the detours of art, those two or three great and simple images in whose presence his heart first opened.",
    "author": "Albert Camus"
  },
  {
    "quote": "Blackness isn't black. It is the last degree of reds. The secret blood of reds.",
    "author": "Hélène Cixous"
  },
  {
    "quote": "Pain and suffering are always inevitable for a large intelligence and a deep heart.",
    "author": "Fyodor Dostoevsky"
  },
  {
    "quote": "Courage is the solution to despair. Reason can provide no answers.",
    "author": "Ernst Toller"
  },
  {
    "quote": "Loneliness does not come from having no people around one, but from being unable to communicate the things that seem important to oneself, or from holding certain views which others find inadmissible.",
    "author": "Carl Jung"
  },
  {
    "quote": "The world of technology thrives best when individuals are left alone to be different, creative, and disobedient.",
    "author": "Don Valentine"
  },
  {
    "quote": "Science is not only compatible with spirituality; it is a profound source of spirituality.",
    "author": "Carl Sagan"
  },
  {
    "quote": "Only after the writer is dead do his words to some extent become disinfected, purified of the accidents of the living body.",
    "author": "Virginia Woolf"
  },
  {
    "quote": "The accumulation of all powers, legislative, executive, and judiciary, in the same hands, whether of one, a few, or many, and whether hereditary, self-appointed, or elective, may justly be pronounced the very definition of tyranny.",
    "author": "James Madison"
  },
  {
    "quote": "Books are finite, sexual encounters are finite, but the desire to read and to fuck is infinite; it surpasses our own deaths, our fears, our hopes for peace.",
    "author": "Roberto Bolaño"
  },
  {
    "quote": "I took no pride in my solitude; but I was dependent on it. The darkness of the room was like sunlight to me.",
    "author": "Charles Bukowski"
  },
  {
    "quote": "There is only one day left, always starting over: It is given to us at dawn and taken away from us at dusk.",
    "author": "Jean-Paul Sartre"
  },
  {
    "quote": "Playfully doing something difficult, whether useful or not, that is hacking.",
    "author": "Richard Stallman"
  },
  {
    "quote": "Be not afraid of growing slowly, be afraid only of standing still.",
    "author": "Chinese Proverb"
  },
  {
    "quote": "If most of us are ashamed of shabby clothes and shoddy furniture, let us be more ashamed of shabby ideas and shoddy philosophies.",
    "author": "Albert Einstein"
  },
  {
    "quote": "The weaker the body, the more imperious its demands; the stronger it is, the better it obeys.",
    "author": "Jean-Jacques Rousseau"
  },
  {
    "quote": "Let me tell you this: if you meet a loner, no matter what they tell you, it's not because they enjoy solitude. It's because they have tried to blend into the world before, and people continue to disappoint them.",
    "author": "Jodi Picoult"
  },
  {
    "quote": "Be careful, lest in casting out your demon you exorcise the best thing in you.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "Perhaps it is true that we do not really exist until there is someone there to see us existing, we cannot properly speak until there is someone who can understand what we are saying in essence, we are not wholly alive until we are loved.",
    "author": "Alain de Botton"
  },
  {
    "quote": "In order to understand the world, one has to turn away from it on occasion.",
    "author": "Albert Camus"
  },
  {
    "quote": "Practical people know that opportunity doesn't knock twice.",
    "author": "Mikhail Bulgakov"
  },
  {
    "quote": "Above all, don't lie to yourself. The man who lies to himself and listens to his own lie comes to a point that he cannot distinguish the truth within him, or around him, and so loses all respect for himself and for others.",
    "author": "Fyodor Dostoyevsky"
  },
  {
    "quote": "I neither complain of the past, nor do I fear the future.",
    "author": "Michel de Montaigne"
  },
  {
    "quote": "To live is to be haunted.",
    "author": "Philip K. Dick"
  },
  {
    "quote": "Violence is the last refuge of the incompetent.",
    "author": "Isaac Asimov"
  },
  {
    "quote": "Excellence is never an accident. It is always the result of high intention, sincere effort, and intelligent execution; it represents the wise choice of many alternatives — choice, not chance, determines your destiny.",
    "author": "Aristotle"
  },
  {
    "quote": "Men spoke of how the heart broke up, but never spoke of how the soul hung speechless in the pause.",
    "author": "James Baldwin"
  },
  {
    "quote": "All books are either dreams or swords.",
    "author": "Amy Lowell"
  },
  {
    "quote": "The mind is beautiful because of the paradox. It uses itself to understand itself.",
    "author": "Adam Elenbaas"
  },
  {
    "quote": "To play without passion is inexcusable.",
    "author": "Ludwig van Beethoven"
  },
  {
    "quote": "The self always consists of two selves; the self that is seen by others, and the self that observes the act.",
    "author": "Hideaki Anno"
  },
  {
    "quote": "I contend that we are both atheists. I just believe in one fewer god than you do. When you understand why you dismiss all the other possible gods, you will understand why I dismiss yours.",
    "author": "Stephen Roberts"
  },
  {
    "quote": "Normality is a paved road: it's comfortable to walk, but no flowers grow.",
    "author": "Vincent van Gogh"
  },
  {
    "quote": "We are described into corners, and then we must describe ourselves out of corners.",
    "author": "Salman Rushdie"
  },
  {
    "quote": "Of course I'll hurt you. Of course you'll hurt me. Of course we will hurt each other. But this is the very condition of existence. To become spring, means accepting the risk of winter. To become presence, means accepting the risk of absence.",
    "author": "Antoine de Saint Exupéry"
  },
  {
    "quote": "Beware the barrenness of a busy life.",
    "author": "Socrates"
  },
  {
    "quote": "The beginning of love is the will to let those we love be perfectly themselves, the resolution not to twist them to fit our own image.",
    "author": "Thomas Merton"
  },
  {
    "quote": "Use laws that are old but food that is fresh.",
    "author": "Periander of Corinth"
  },
  {
    "quote": "Hardly ever does one think of oneself, but only how to escape from oneself.",
    "author": "Marcel Proust"
  },
  {
    "quote": "The greatest mistake you can make in life is to continually be afraid you will make one.",
    "author": "Elbert Hubbard"
  },
  {
    "quote": "There is no greater agony than bearing an untold story inside you.",
    "author": "Maya Angelou"
  },
  {
    "quote": "Knowing your own darkness is the best method for dealing with the darknesses of other people.",
    "author": "Carl Jung"
  },
  {
    "quote": "Taking a new step, uttering a new word, is what people fear most.",
    "author": "Fyodor Dostoyevsky"
  },
  {
    "quote": "Introspection does not need to be a still life. It can be an active alchemy.",
    "author": "Anaïs Nin"
  },
  {
    "quote": "No one loses anyone, because no one owns anyone. That is the true experience of freedom: having the most important thing in the world without owning it.",
    "author": "Paulo Coelho"
  },
  {
    "quote": "Be like water making its way through cracks. Do not be assertive, but adjust to the object, and you shall find a way around or through it. If nothing within you stays rigid, outward things will disclose themselves.",
    "author": "Bruce Lee"
  },
  {
    "quote": "Take the risk of thinking for yourself. Much more happiness, truth, beauty, and wisdom will come to you that way.",
    "author": "Christopher Hitchens"
  },
  {
    "quote": "I prefer nothing, unless it is true.",
    "author": "Socrates"
  },
  {
    "quote": "The Universe continues to be in the present tense.",
    "author": "Ibn Arabi"
  },
  {
    "quote": "The philosophy of six thousand years has not searched the chambers and magazines of the soul. In its experiments there has always remained, in the last analysis, a residuum it could not resolve. Humanity is a stream whose source is hidden.",
    "author": "Ralph Waldo Emerson"
  },
  {
    "quote": "Here is the riddle of love: Everything it gives to you, it takes away.",
    "author": "Alice Hoffman"
  },
  {
    "quote": "The limits of my language are the limits of my mind. All I know is what I have words for.",
    "author": "Ludwig Wittgenstein"
  },
  {
    "quote": "We are anxiety-ridden animals. Our minds are continually active, fabricating an anxious, usually self-preoccupied, often falsifying veil which partially conceals the world.",
    "author": "Iris Murdoch"
  },
  {
    "quote": "Philosophy begins in wonder. And, at the end, when philosophic thought has done its best, the wonder remains.",
    "author": "Alfred North Whitehead"
  },
  {
    "quote": "When you are crazy you learn to keep quiet.",
    "author": "Philip K. Dick"
  },
  {
    "quote": "Where you are understood, you are at home. Understanding nourishes belonging. When you really feel understood, you feel free to release yourself into the trust and shelter of the other person's soul.",
    "author": "John O'Donohue"
  },
  {
    "quote": "You could do anything, if only you dared. And deep down, you know it, too. That's what scares you most.",
    "author": "Sara J. Maas"
  },
  {
    "quote": "The forests grow back with patience, not rage.",
    "author": "Tony Hoagland"
  },
  {
    "quote": "Forces that we cannot understand permeate our universe. We see the shadows of those forces when they are projected upon a screen available to our senses, but understand them we do not… Understanding requires words. Some things cannot be reduced to words. There are things that can only be experienced wordlessly.",
    "author": "Frank Herbert"
  },
  {
    "quote": "Sleep tries to seduce me by promising a more reasonable tomorrow.",
    "author": "Elizabeth Smart"
  },
  {
    "quote": "The free man is he who does not fear to go to the end of his thought.",
    "author": "Leon Blum"
  },
  {
    "quote": "Complexity is the worst enemy of security. The more complex a system is, the less secure it is.",
    "author": "Bruce Schneier"
  },
  {
    "quote": "What would life be if we had no courage to attempt anything?",
    "author": "Vincent van Gogh"
  },
  {
    "quote": "When faced with the inevitable, counter with the improbable.",
    "author": "Stacy Schiff"
  },
  {
    "quote": "We have to do God's work, because God's not going to.",
    "author": "Penn Gillette"
  },
  {
    "quote": "It is better to act and repent than not to act and regret.",
    "author": "Niccolò Machiavelli"
  },
  {
    "quote": "Have no fear of perfection, you'll never reach it.",
    "author": "Salvador Dali"
  },
  {
    "quote": "You're always haunted by the idea you're wasting your life.",
    "author": "Chuck Palahniuk"
  },
  {
    "quote": "In another world we shall understand it all.",
    "author": "Leo Tolstoy"
  },
  {
    "quote": "The mind is its own place, and in itself can make a heaven of hell, a hell of heaven.",
    "author": "John Milton"
  },
  {
    "quote": "In the end, the most interesting people always leave.",
    "author": "Paulo Coelho"
  },
  {
    "quote": "If you don't at least occasionally contradict yourself, your position isn't nearly complex enough.",
    "author": "Terence McKenna"
  },
  {
    "quote": "I can't go back to yesterday because I was a different person then.",
    "author": "Lewis Carroll"
  },
  {
    "quote": "To forget the dead would be akin to killing them a second time.",
    "author": "Elie Wiesel"
  },
  {
    "quote": "There are two ways to slice easily through life; to believe everything or to doubt everything. Both ways save us from thinking.",
    "author": "Alfred Korzybski"
  },
  {
    "quote": "That's how you know you love someone, I guess, when you can't experience anything without wishing the other person were there to see it, too.",
    "author": "Kaui Hart Hemmings"
  },
  {
    "quote": "Beauty will save the world.",
    "author": "Fyodor Dostoyevsky"
  },
  {
    "quote": "If you don't know, the thing to do is not to get scared, but to learn.",
    "author": "Ayn Rand"
  },
  {
    "quote": "Our lives are defined by opportunities, even the ones we miss.",
    "author": "F. Scott Fitzgerald"
  },
  {
    "quote": "Life shrinks or expands in proportion to one's courage.",
    "author": "Anaïs Nin"
  },
  {
    "quote": "The silent majority should not be silent because they can lose to the vocal minority and self-interested politicians.",
    "author": "Robert Mujica"
  },
  {
    "quote": "Darkness is to space what silence is to sound; i.e., the interval.",
    "author": "Marshall McLuhan"
  },
  {
    "quote": "I tell myself I am searching for something. But more and more, it feels like I am wandering, waiting for something to happen to me, something that will change everything, something that my whole life has been leading up to.",
    "author": "Khaled Hosseini"
  },
  {
    "quote": "The first lesson of economics is scarcity: there is never enough of anything to fully satisfy all those who want it. The first lesson of politics is to disregard the first lesson of economics.",
    "author": "Thomas Sowell"
  },
  {
    "quote": "People will forget what you said, people will forget what you did, but people will never forget how you made them feel.",
    "author": "Carl Buehner"
  },
  {
    "quote": "If we spoke a different language, we would perceive a somewhat different world.",
    "author": "Ludwig Wittgenstein"
  },
  {
    "quote": "Imagination is the golden-eyed monster that never sleeps. It must be fed; it cannot be ignored.",
    "author": "Patricia A. McKillip"
  },
  {
    "quote": "It is the things you cannot see coming that are strong enough to kill you.",
    "author": "Jodi Picoult"
  },
  {
    "quote": "The aim is to balance the terror of being alive with the wonder of being alive.",
    "author": "Carlos Castaneda"
  },
  {
    "quote": "The trouble with having an open mind, of course, is that people will insist on coming along and trying to put things in it.",
    "author": "Terry Pratchett"
  },
  {
    "quote": "Our current mental-hygiene philosophy stresses the idea that people ought to be happy, that unhappiness is a symptom of maladjustment. Such a value system might be responsible for the fact that the burden of unavoidable unhappiness is increased by unhappiness about being unhappy.",
    "author": "Viktor E. Frankl"
  },
  {
    "quote": "Friendship is unnecessary, like philosophy, like art…. It has no survival value; rather it is one of those things which give value to survival.",
    "author": "C.S. Lewis"
  },
  {
    "quote": "Our existence is but a brief crack of light between two eternities of darkness.",
    "author": "Vladimir Nabokov"
  },
  {
    "quote": "To go wrong in one's own way is better than to go right in someone else's.",
    "author": "Fyodor Dostoyevsky"
  },
  {
    "quote": "Waiting is painful. Forgetting is painful. But not knowing which to do is the worst kind of suffering.",
    "author": "Paulo Coelho"
  },
  {
    "quote": "You're told that you're in your head too much, a phrase that's often deployed against the quiet and cerebral. Or maybe there's another word for such people: thinkers.",
    "author": "Susan Cain"
  },
  {
    "quote": "*J'aurais dû être plus gentile* — I should have been more kind. That is something a person will never regret. You will never say to yourself when you are old, Ah, I wish I was not good to that person. You will never think that.",
    "author": "Khaled Hosseini"
  },
  {
    "quote": "It's only terrible to have nothing to wait for.",
    "author": "Erich Maria Remarque"
  },
  {
    "quote": "You are more than a list of mistakes and if anyone tells you otherwise, let it be the last they make.",
    "author": "Iain Thomas"
  },
  {
    "quote": "The essence of the independent mind lies not in what it thinks, but in how it thinks.",
    "author": "Christopher Hitchens"
  },
  {
    "quote": "I don't know what's worse: to not know what you are and be happy, or to become what you've always wanted to be, and feel alone.",
    "author": "Daniel Keyes"
  },
  {
    "quote": "Some struggles are so solitary that they drown in words.",
    "author": "Martha Manning"
  },
  {
    "quote": "Not living your life is just like killing yourself, only it takes longer.",
    "author": "Amy Sarig King"
  },
  {
    "quote": "People think being alone makes you lonely, but I don't think that's true. Being surrounded by the wrong people is the loneliest thing in the world.",
    "author": "Kim Culbertson"
  },
  {
    "quote": "Read a thousand books, and your words will flow like a river.",
    "author": "Lisa See"
  },
  {
    "quote": "You do not write your life with words…You write it with actions. What you think is not important. It is only important what you do.",
    "author": "Patrick Ness"
  },
  {
    "quote": "No matter how isolated you are and how lonely you feel, if you do your work truly and conscientiously, unknown friends will come and seek you.",
    "author": "Carl Jung"
  },
  {
    "quote": "I hope that whoever you are, you escape this place. I hope that the world turns and that things get better.",
    "author": "Alan Moore"
  },
  {
    "quote": "Forget about life, forget about worrying about right and wrong. Plunge into the unknown and the endless and find your place there.",
    "author": "Zhuangzi"
  },
  {
    "quote": "We are building the nervous sytem of the human oversoul.",
    "author": "Terence McKenna"
  },
  {
    "quote": "Our entire universe is contained in the mind and the spirit. We may choose not to find access to it, we may even deny its existence, but it is indeed there inside us.",
    "author": "Alexander Shulgin"
  },
  {
    "quote": "Music carries the weight of being human, takes it away so you don't have to think at all, you just have to listen. Music tells every story there is.",
    "author": "Estelle Laure"
  },
  {
    "quote": "It's a most distressing affliction to have a sentimental heart and a skeptical mind.",
    "author": "Nagulb Mafouz"
  },
  {
    "quote": "I affect their imagination. It is the strongest power.",
    "author": "Anaïs Nin"
  },
  {
    "quote": "The most formidable weapon against errors of every kind is reason. I have never used any other, and I trust I never shall.",
    "author": "Thomas Paine"
  },
  {
    "quote": "The mind is not a vessel to be filled, but a fire to be kindled.",
    "author": "Plutarch"
  },
  {
    "quote": "To burn with desire and keep quiet about it is the greatest punishment we can bring on ourselves. ",
    "author": "Federico García Lorca"
  },
  {
    "quote": "Nature is not a place to visit. It is home.",
    "author": "Gary Snyder"
  },
  {
    "quote": "What can be asserted without evidence can also be dismissed without evidence.",
    "author": "Christopher Hitchens"
  },
  {
    "quote": "You have power over your mind - not outside events. Realize this, and you will find strength.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "Real happiness is impossible without solitude. The fallen angel betrayed God probably because he longed for solitude.",
    "author": "Anton Chekhov"
  },
  {
    "quote": "In a work of art, chaos must shimmer through the veil of order.",
    "author": "Novalis"
  },
  {
    "quote": "Nothing beautiful without struggle.",
    "author": "Plato"
  },
  {
    "quote": "The life of the spirit comes from the prior death of the mind. If people can kill the mind, the original comes alive. Killing the mind does not mean quietism, it means undivided concentration.",
    "author": "Lü Dongbin"
  },
  {
    "quote": "Everyone sees what you appear to be, few experience what you really are.",
    "author": "Niccolò Machiavelli"
  },
  {
    "quote": "You will never be able to experience everything. So, please, do poetical justice to your soul and simply experience yourself.",
    "author": "Albert Camus"
  },
  {
    "quote": "What makes Heroic? To face simultaneously one's greatest suffering and one's highest hope.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "The problem was precision, perfection; the problem was digitization, which sucked the life out of everything that got smeared through its microscopic mesh. Film, photography, music: dead.",
    "author": "Jennifer Egan"
  },
  {
    "quote": "People were created to be loved. Things were created to be used. The reason why the world is in chaos is because things are being loved and people are being used.",
    "author": "Emmanuel Torres"
  },
  {
    "quote": "The only lies for which we are truly punished are those we tell ourselves.",
    "author": "V. S. Naipaul"
  },
  {
    "quote": "The first principle is that you must not fool yourself - and you are the easiest person to fool.",
    "author": "Richard Feynman"
  },
  {
    "quote": "What is necessary, after all, is only this: solitude, vast inner solitude. To walk inside yourself and meet no one for hours—that is what you must be able to attain.",
    "author": "Rainer Maria Rilke"
  },
  {
    "quote": "We are faced with the paradoxical fact that education has become one of the chief obstacles to intelligence and freedom of thought.",
    "author": "Bertrand Russel"
  },
  {
    "quote": "Every true love and friendship is a story of unexpected transformation. If we are the same person before and after we loved, that means we haven't loved enough.",
    "author": "Elif Shafak"
  },
  {
    "quote": "The books that the world calls immoral are books that show the world its own shame.",
    "author": "Oscar Wilde"
  },
  {
    "quote": "We are far more imprisoned by cultural conventions than we are by physical laws.",
    "author": "Terence McKenna"
  },
  {
    "quote": "The thinker needs no one to refute him, for that he has himself.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "It is a thousand pities never to say what one feels.",
    "author": "Virginia Woolf"
  },
  {
    "quote": "Have you considered that if you don't make waves, nobody including yourself will know that you are alive?",
    "author": "Theodore Rubin"
  },
  {
    "quote": "Don't ask yourself what the world needs. Ask yourself what makes you come alive, and go do that, because what the world needs is people who have come alive.",
    "author": "Howard Thurman"
  },
  {
    "quote": "The only journey is the one within.",
    "author": "Rainer Maria Rilke"
  },
  {
    "quote": "Orthodoxy is the diehard of the world of thought. It learns not, neither can it forget.",
    "author": "Aldous Huxley"
  },
  {
    "quote": "If we had a keen vision and feeling of all ordinary human life, it would be like hearing the grass grow and the squirrel's heart beat, and we should die of that roar which lies on the other side of silence.",
    "author": "George Eliot"
  },
  {
    "quote": "Until you make the unconscious conscious, it will direct your life and you will call it fate.",
    "author": "Carl Jung"
  },
  {
    "quote": "Change is one thing, progress is another. 'Change' is scientific, 'progress' is ethical; change is indubitable, whereas progress is a matter of controversy.",
    "author": "Bertrand Russell"
  },
  {
    "quote": "At one point consciousness-altering devices like the microscope and telescope were criminalized for exactly the same reason that psychedelic plants were banned in later years. They allow us to peer into bits and zones of Chaos.",
    "author": "Timothy Leary"
  },
  {
    "quote": "But there was no need to be ashamed of tears, for tears bore witness that a man had the greatest of courage, the courage to suffer.",
    "author": "Viktor E. Frankl"
  },
  {
    "quote": "Intelligence is the capacity to be reborn again and again.",
    "author": "Osho"
  },
  {
    "quote": "Never confuse faith, or belief — of any kind — with something even remotely intellectual.",
    "author": "John Irving"
  },
  {
    "quote": "Life, whatever it is, is an opportunity.",
    "author": "Terence McKenna"
  },
  {
    "quote": "Mere precedent is a dangerous source of authority.",
    "author": "Andrew Jackson"
  },
  {
    "quote": "Convictions are more dangerous enemies of truth than lies.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "It is no measure of health to be well adjusted to a profoundly sick society.",
    "author": "Jiddu Krishnamurti"
  },
  {
    "quote": "There are decades where nothing happens; and there are weeks where decades happen.",
    "author": "Vladimir Lenin"
  },
  {
    "quote": "You are not naked when you take off your clothes. You still wear your religious assumptions, your prejudices, your fears, your illusions, your delusions. When you shed the cultural operating system, then essentially you stand naked before the inspection of your own psyche.",
    "author": "Terence McKenna"
  },
  {
    "quote": "Adopt the pace of nature: her secret is patience.",
    "author": "Ralph Waldo Emerson"
  },
  {
    "quote": "Adults follow paths. Children explore.",
    "author": "Neil Gaiman"
  },
  {
    "quote": "I would rather die of passion than of boredom.",
    "author": "Vincent van Gogh"
  },
  {
    "quote": "If the highest aim of a captain was to preserve his ship, he would keep it in port forever.",
    "author": "Thomas Aquinas"
  },
  {
    "quote": "Close your eyes and let the mind expand. Let no fear of death or darkness arrest its course. Allow the mind to merge with Mind. Let it flow out upon the great curve of consciousness. Let it soar on the wings of the great bird of duration, up to the very Circle of Eternity.",
    "author": "Hermes Trismegistus"
  },
  {
    "quote": "We learn from history that we do not learn from history.",
    "author": "Georg Hegel"
  },
  {
    "quote": "The individual's duty is to do what he wants to do, to think whatever he likes, to be accountable to no one but himself, to challenge every idea and every person.",
    "author": "Jean-Paul Sartre"
  },
  {
    "quote": "A warrior considers himself already dead, so there is nothing to lose. The worst has already happened to him, therefore he's clear and calm; judging him by his acts or by his words, one would never suspect that he has witnessed everything.",
    "author": "Carlos Castaneda"
  },
  {
    "quote": "The Net interprets censorship as damage and routes around it.",
    "author": "John Gilmore"
  },
  {
    "quote": "All forms of love, suffering, and madness. He [the poet] searches himself. He exhausts all poisons in himself and keeps only their quintessences.",
    "author": "Arthur Rimbaud"
  },
  {
    "quote": "More important than the road we travel is who we become along the way.",
    "author": "David J. Lieberman"
  },
  {
    "quote": "There are things known and things unknown and in between are the doors of perception.",
    "author": "Aldous Huxley"
  },
  {
    "quote": "If only there were evil people somewhere insidiously committing evil deeds and it were necessary only to separate them from the rest of us and destroy them. But the line dividing good and evil cuts through the heart of every human being. And who is willing to destroy a piece of his own heart?",
    "author": "Alexander Solzhenitsyn"
  },
  {
    "quote": "Simplicity, patience, compassion. These three are your greatest treasures.",
    "author": "Lao Tzu"
  },
  {
    "quote": "Eroticism is first and foremost a thirst for otherness; and the supernatural is the supreme otherness.",
    "author": "Octavio Paz"
  },
  {
    "quote": "Our growth depends not on how many experiences we devour, but on how many we digest.",
    "author": "Ralph W. Sockman"
  },
  {
    "quote": "The life of the spirit comes from the prior death of the mind. If people can kill the mind, the original comes alive. Killing the mind does not mean quietism, it means undivided concentration.",
    "author": "Lü Dongbin"
  },
  {
    "quote": "Belief is the wound that knowledge heals.",
    "author": "Ursula K. Le Guin"
  },
  {
    "quote": "Be. And, at the same time, know what it is not to be. That emptiness inside you allows you to vibrate in resonance with your world. Use it for once.",
    "author": "Rainer Maria Rilke"
  },
  {
    "quote": "Creation and destruction are one, to the eyes who can see beauty.",
    "author": "Bhagavad Gita"
  },
  {
    "quote": "It's foolhardy to think that a story ends.",
    "author": "Madeleine Thien"
  },
  {
    "quote": "The only way to deal with an unfree world is to become so absolutely free that your very existence is an act of rebellion.",
    "author": "Albert Camus"
  },
  {
    "quote": "He who cannot obey himself will be commanded.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "Silence is no weakness of language. It is, on the contrary, its strength. It is the weakness of words not to know this.",
    "author": "Edmond Jabès"
  },
  {
    "quote": "Better to die on one's feet than to live on one's knees.",
    "author": "Jean-Paul Sartre"
  },
  {
    "quote": "I think that modern physics has definitely decided in favor of Plato. In fact the smallest units of matter are not physical objects in the ordinary sense; they are forms, ideas which can be expressed unambiguously only in mathematical language.",
    "author": "Werner Heisenberg"
  },
  {
    "quote": "Political correctness is fascism pretending to be manners.",
    "author": "George Carlin"
  },
  {
    "quote": "The brain is not a blind, reactive machine, but a complex, sensitive biocomputer that we can program. And if we don't take the responsibility for programming it, then it will be programmed unwittingly by accident or by the social environment.",
    "author": "Timothy Leary"
  },
  {
    "quote": "Think in the morning. Act in the noon. Eat in the evening. Sleep in the night.",
    "author": "William Blake"
  },
  {
    "quote": "Not being heard is no reason for silence.",
    "author": "Victor Hugo"
  },
  {
    "quote": "It is far better to be alone, than to be in bad company.",
    "author": "George Washington"
  },
  {
    "quote": "With rebellion, awareness is born.",
    "author": "Albert Camus"
  },
  {
    "quote": "Theologians may quarrel, but the mystics of the world speak the same language.",
    "author": "Meister Eckhart"
  },
  {
    "quote": "A disciple asked of his master: 'You teach me ways of war yet you often preach of peace, how do these lessons find harmony?' The master replied: 'It is better to be a warrior in a garden than a gardener in a war.'",
    "author": "Sun Tzu"
  },
  {
    "quote": "Knowledge divorced from justice is cunning, not wisdom.",
    "author": "Plato"
  },
  {
    "quote": "Any life, however long and complicated it may be, actually consists of a single moment — the moment when a man knows forever more who he is.",
    "author": "Jorge Luis Borges"
  },
  {
    "quote": "Culture is a mass hallucination, and when you step outside the mass hallucination you see it for what it's worth.",
    "author": "Terence McKenna"
  },
  {
    "quote": "The only journey is the one within.",
    "author": "Rainer Maria Rilke"
  },
  {
    "quote": "Eras are conveniences, particularly for those who never experienced them. We carve history from totalities beyond our grasp.",
    "author": "William Gibson"
  },
  {
    "quote": "We shape our tools and thereafter our tools shape us.",
    "author": "Marshall McLuhan"
  },
  {
    "quote": "Those who'll play with cats must expect to be scratched.",
    "author": "Miguel de Cervantes"
  },
  {
    "quote": "All of humanity's problems come from man's inability to sit quietly in a room alone.",
    "author": "Blaise Pascal"
  },
  {
    "quote": "Men must live and create. Live to the point of tears.",
    "author": "Albert Camus"
  },
  {
    "quote": "Well behaved women rarely make history.",
    "author": "Eleanor Roosevelt"
  },
  {
    "quote": "For small creatures such as we, the vastness is bearable only through love.",
    "author": "Carl Sagan"
  },
  {
    "quote": "When you reach the end of what you should know, you will be at the beginning of what you should sense.",
    "author": "Kahlil Gibran"
  },
  {
    "quote": "One keeps forgetting to go down to the foundations. One doesn't put the question marks deep down enough.",
    "author": "Ludwig Wittgenstein"
  },
  {
    "quote": "You make yourself strong because it's expected of you. You become confident because someone beside you is unsure. You turn into the person others need you to be.",
    "author": "Jodi Picoult"
  },
  {
    "quote": "There is in man a deep so deep it is hidden even to him in whom it is.",
    "author": "Augustine"
  },
  {
    "quote": "It is not enough to conquer, one must learn to seduce.",
    "author": "Voltaire"
  },
  {
    "quote": "Philosophy calls for simple living, not for doing penance, and the simple way of life need not be a crude one.",
    "author": "Seneca"
  },
  {
    "quote": "He who is brave is free.",
    "author": "Lucius Annaeus Seneca"
  },
  {
    "quote": "It is better to light one small candle than to curse the darkness.",
    "author": "Confucius"
  },
  {
    "quote": "The cure for anything is salt water — sweat, tears, or the sea.",
    "author": "Isak Dinesen"
  },
  {
    "quote": "There is nothing in this world that belongs to us in an absolute sense.",
    "author": "José Saramago"
  },
  {
    "quote": "Today I escaped from anxiety. Or no, I discarded it, because it was within me, in my own perceptions — not outside.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "Mostly it is loss which teaches us about the worth of things.",
    "author": "Arthur Schopenhauer"
  },
  {
    "quote": "Characteristics of the rational soul: self-perception, self-examination, and the power to make of itself whatever it wants.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "Giving up something that no longer serves a purpose, or protects you, or helps you, isn't giving up at all, it's growing up.",
    "author": "Laurell K. Hamilton"
  },
  {
    "quote": "The mountains of truth you will never climb in vain.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "It is better, by yielding to truth, to conquer opinion; than, by yielding to opinion, to be defeated by truth.",
    "author": "Epictetus"
  },
  {
    "quote": "For myths are to religion what poetry is to truth: ridiculous masks laid upon the passion to live.",
    "author": "Albert Camus"
  },
  {
    "quote": "I must change my life so that I can live it, not wait for it.",
    "author": "Susan Sontag"
  },
  {
    "quote": "It is in the very nature of the human condition that each new generation grows into an old world, so that to prepare a new generation for a new world can only mean that one wishes to strike from the newcomers' hands their own chance at the new.",
    "author": "Hannah Arendt"
  },
  {
    "quote": "If your daily life seems poor, do not blame it; blame yourself, tell yourself that you are not poet enough to call forth its riches; for to the creator there is no poverty and no poor indifferent place.",
    "author": "Rainer Maria Rilke"
  },
  {
    "quote": "Your mind will take on the character of your most frequent thoughts: souls are dyed by thoughts.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "Only very few people live. Ninety-nine point nine percent of people slowly commit suicide.",
    "author": "Osho"
  },
  {
    "quote": "The world of immediate experience — the world in which we find ourselves living — must be comprehended, transformed, even subverted in order to become that which it really is.",
    "author": "Herbert Marcuse"
  },
  {
    "quote": "No one is accountable for existing at all, or for being constituted as he is, or for living in the circumstances and surroundings in which he lives. The fatality of his nature cannot be disentangled from the fatality of all that which has been and will be.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "I talk about the gods, I am an atheist. But I am an artist too, and therefore a liar. Distrust everything I say. I am telling the truth.",
    "author": "Ursula K. Le Guin"
  },
  {
    "quote": "Sorrow is concealed in gilded palaces, and there's no escaping it.",
    "author": "Fyodor Dovstoevsky"
  },
  {
    "quote": "The girl raised her eyes to see who was passing by the window, and that casual glance was the beginning of a cataclysm of love that still had not ended half a century later.",
    "author": "Gabriel García Márquez"
  },
  {
    "quote": "Because the world is so full of death and horror, I try again and again to console my heart and pick the flowers that grow in the midst of hell.",
    "author": "Hermann Hesse"
  },
  {
    "quote": "We are what we pretend to be, so we must be careful about what we pretend to be.",
    "author": "Kurt Vonnegut"
  },
  {
    "quote": "The lack of your best hurts everything.",
    "author": "Jordan Peterson"
  },
  {
    "quote": "A man cannot step twice into the same river. Because it is not the same river, and he is not the same man.",
    "author": "Heraclitus"
  },
  {
    "quote": "Psychedelics are illegal not because a loving government is concerned that you may jump out of a third story window. Psychedelics are illegal because they dissolve opinion structures and culturally laid down models of behavior and information processing.",
    "author": "Terence Mckenna"
  },
  {
    "quote": "The price of anything is the amount of life you exchange for it.",
    "author": "Henry David Thoreau"
  },
  {
    "quote": "Almost nothing material is needed for a happy life, for he who has understood existence.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "If you look at the drug war from a purely economic point of view, the role of the government is to protect the drug cartel.",
    "author": "Milton Friedman"
  },
  {
    "quote": "However many times you fail in businesses, you only need to be right once—and then everyone calls you an overnight success, and you laugh your way to the bank.",
    "author": "Mark Cuban"
  },
  {
    "quote": "If it can be destroyed by the truth, it deserves to be destroyed by the truth.",
    "author": "Carl Sagan"
  },
  {
    "quote": "Although the world is full of suffering, it is also full of overcoming of it.",
    "author": "Helen Keller"
  },
  {
    "quote": "One of the main functions of religious structures is to protect people against a direct experience of God.",
    "author": "Carl Jung"
  },
  {
    "quote": "Anti-intellectualism has been a constant thread winding its way through our political and cultural life, nurtured by the false notion that democracy means that 'my ignorance is just as good as your knowledge.'",
    "author": "Isaac Asimov"
  },
  {
    "quote": "Don't let your imagination be crushed by life as a whole. Don't try to picture everything bad that could possibly happen. Stick with the situation at hand.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "First, get the facts, then you can distort them at your leisure.",
    "author": "Mark Twain"
  },
  {
    "quote": "Every ideology is contrary to human psychology.",
    "author": "Albert Camus"
  },
  {
    "quote": "Miss a meal of you have to, but don't miss a book.",
    "author": "Jim Rohn"
  },
  {
    "quote": "Disgraceful if, in this life where your body does not fail, your soul should fail you first.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "Man's obedience to his own genius is the ultimate definition of faith.",
    "author": "Ralph Waldo Emerson"
  },
  {
    "quote": "It is no small art to sleep: for that purpose you must keep awake all day.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "Absolute freedom mocks at justice. Absolute justice denies freedom. To be fruitful, the two ideas must find their limits in each other.",
    "author": "Albert Camus"
  },
  {
    "quote": "Vanity is the greatest seducer of reason: when you are most convinced that your work is important, that is when you are most under its spell.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "Most people learn nothing from experience, except confirmation of their prejudices.",
    "author": "Bertrand Russell"
  },
  {
    "quote": "We are slowed down sound and light waves, a walking bundle of frequencies tuned into the cosmos. We are souls dressed up in sacred biochemical garments and our bodies are the instruments through which our souls play their music.",
    "author": "Albert Einstein"
  },
  {
    "quote": "If people are moored shallowly then storms wreck them. And storms come along.",
    "author": "Jordan B. Peterson"
  },
  {
    "quote": "Yet the timeless in you is aware of life's timelessness, and knows that yesterday is but today's memory and tomorrow is today's dream.",
    "author": "Khalil Gibran"
  },
  {
    "quote": "In the fields with which we are concerned knowledge exists only in lightning flashes. The text is the thunder rolling long afterwards.",
    "author": "Walter Benjamin"
  },
  {
    "quote": "It stands plain as a wardrobe, what we know, have always known, know that we can't escape, yet can't accept. One side will have to go.",
    "author": "Philip Larkin"
  },
  {
    "quote": "Boredom is essential for writers; it is the only time they get to write.",
    "author": "Andrew Sean Greer"
  },
  {
    "quote": "There is no reality except in action.",
    "author": "Jean-Paul Sartre"
  },
  {
    "quote": "The world sighs toward me in a long rhythm, and brings me the peace and indifference of immortal things.",
    "author": "Albert Camus"
  },
  {
    "quote": "It's ridiculous to defend yourself against an accusation that has no ground.",
    "author": "Jordan B. Peterson"
  },
  {
    "quote": "True happiness is not found in amusement.",
    "author": "Aristotle"
  },
  {
    "quote": "Those who grasp after friendship and those who shrink from it are not worthy of approval; on the other hand, it is necessary to risk some pleasure for the pleasures of friendship.",
    "author": "Epicurus"
  },
  {
    "quote": "Money, it turned out, was exactly like sex. You thought of nothing else if you didn't have it and thought of other things if you did.",
    "author": "James Baldwin"
  },
  {
    "quote": "There are some people who could hear you speak a thousand words, and still not understand you. And there are others who will understand — without you even speaking a word.",
    "author": "Yasmin Mogahed"
  },
  {
    "quote": "The only completely consistent people are the dead.",
    "author": "Aldous Huxley"
  },
  {
    "quote": "What happens when you let an unsatisfactory present go on long enough? It becomes your entire history.",
    "author": "Louise Erdrich"
  },
  {
    "quote": "The illusion is reality. The only contradiction is the observer.",
    "author": "Lionel Suggs"
  },
  {
    "quote": "If home can't be where you come from, then home is what you make of where you go.",
    "author": "Eleanor Catton"
  },
  {
    "quote": "Let no man be ashamed to speak what he is not ashamed to think.",
    "author": "Michel de Montaigne"
  },
  {
    "quote": "Modern politics cannot be a matter of genuine moral consensus. And it is not. Modern politics is civil war carried on by other means.",
    "author": "Alasdair MacIntyre"
  },
  {
    "quote": "Poetry is a naked woman, a naked man, and the distance between them.",
    "author": "Lawrence Ferlinghetti"
  },
  {
    "quote": "A clever person solves a problem. A wise person avoids it.",
    "author": "Albert Einstein"
  },
  {
    "quote": "Tactics is knowing what to do when there is something to do; Strategy is knowing what to do when there is nothing to do.",
    "author": "Savielly Tartakower"
  },
  {
    "quote": "A reader lives a thousand lives before he dies. The man who never reads lives only one.",
    "author": "George R. R. Martin"
  },
  {
    "quote": "By all means marry; if you get a good wife you'll be happy. If you get a bad one you'll become a philosopher.",
    "author": "Socrates"
  },
  {
    "quote": "When all is said and done, what is clear is that all lives end before their time.",
    "author": "José Saramago"
  },
  {
    "quote": "All thinking men are atheists.",
    "author": "Ernest Hemingway"
  },
  {
    "quote": "Perhaps all the dragons in our lives are princesses who are only waiting to see us act, just once, with beauty and courage. Perhaps everything that frightens us is, in its deepest essence, something helpless that wants our love.",
    "author": "Rainer Maria Rilke"
  },
  {
    "quote": "The more you love a memory the stronger and stranger it becomes.",
    "author": "Vladimir Nabokov"
  },
  {
    "quote": "I didn't make for an interesting person. I didn't want to be interesting, it was too hard. What I really wanted was only a soft, hazy space to live in, and to be left alone.",
    "author": "Charles Bukowski"
  },
  {
    "quote": "The man who makes no mistakes does not usually make anything.",
    "author": "Theodore Roosevelt"
  },
  {
    "quote": "So long as you write what you wish to write, that is all that matters; and whether it matters for ages or only for hours, nobody can say.",
    "author": "Virginia Woolf"
  },
  {
    "quote": "Sex relieves tension — love causes it.",
    "author": "Woody Allen"
  },
  {
    "quote": "Such as are your habitual thoughts, such also will be the character of your mind; for the soul is dyed by the thoughts.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "A savage desire for strong emotions and sensations burns inside me: a rage against this soft-tinted, shallow, standardized and sterilized life, and a mad craving to smash something up, a department store, say, or a cathedral, or myself.",
    "author": "Herman Hesse"
  },
  {
    "quote": "As for me, I longed to love as people long to cry. I felt that every hour I slept now would be an hour stolen from life … that is to say from those hours of undefined desire.",
    "author": "Albert Camus"
  },
  {
    "quote": "The quantum is that embarrassing little piece of thread that always hangs from the sweater of space-time. Pull it and the whole thing unravels.",
    "author": "Fred Alan Wolf"
  },
  {
    "quote": "I just want to sleep. A coma would be nice. Or amnesia. Anything, just to get rid of this, these thoughts, whispers in my mind.",
    "author": "Laurie Halse Anderson"
  },
  {
    "quote": "Only he who never plays, never loses.",
    "author": "Anatoly Karpov"
  },
  {
    "quote": "If life becomes hard to bear we think of a change in our circumstances. But the most important and effective change, a change in our own attitude, hardly even occurs to us, and the resolution to take such a step is very difficult for us.",
    "author": "Ludwig Wittgenstein"
  },
  {
    "quote": "There is nothing more necessary than truth; in comparison with it, everything else has only secondary value.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "I couldn't help wondering if that was what God put me on Earth for – to find out how much a man could take without breaking.",
    "author": "Kurt Vonnegut"
  },
  {
    "quote": "A gentleman is one who puts more into the world than he takes out.",
    "author": "George Bernard Shaw"
  },
  {
    "quote": "It is not, as a rule, by means of useful inventions, or of any other action which increases the general wealth of the community, that men amass fortunes; it is much more often by skill in exploiting or circumventing others.",
    "author": "Bertrand Russell"
  },
  {
    "quote": "No nation was ever so virtuous as each believes itself, and none was ever so wicked as each believes the other.",
    "author": "Bertrand Russell"
  },
  {
    "quote": "It's enough for me to be sure that you and I exist at this moment.",
    "author": "Gabriel García Márquez"
  },
  {
    "quote": "Everything electronic is trying to add dimensionality to itself.",
    "author": "Terence McKenna"
  },
  {
    "quote": "It requires a very unusual mind to undertake the analysis of the obvious.",
    "author": "Alfred North Whitehead "
  },
  {
    "quote": "The silent majority is silent because it has nothing to say. It's time for the silent majority to speak. And when we do, things will change; but not until.",
    "author": "David Icke"
  },
  {
    "quote": "Have self-respect in your own uniqueness, and don't let anyone take it away.",
    "author": "David Icke"
  },
  {
    "quote": "As long as I can communicate, I can create. As long as I can create, I am free.",
    "author": "Lewis Hill"
  },
  {
    "quote": "If you make people think they're thinking, they'll love you; but if you really make them think, they'll hate you.",
    "author": "Don Marquis"
  },
  {
    "quote": "The alms given to a naked man in the street do not fulfill the obligations of the state, which owes to every citizen a certain subsistence, a proper nourishment, convenient clothing, and a kind of life not incompatible with health.",
    "author": "Charles de Montesquieu"
  },
  {
    "quote": "Below a certain point, if you keep too quiet, people no longer see you as thoughtful or deep; they simply forget you.",
    "author": "Douglas Coupland"
  },
  {
    "quote": "There is no avoiding war; it can only be postponed.",
    "author": "Niccolò Machiavelli"
  },
  {
    "quote": "Destiny does not send us heralds. She is too wise or too cruel for that.",
    "author": "Oscar Wilde"
  },
  {
    "quote": "I have love in me the likes of which you can scarcely imagine and rage the likes of which you would not believe. If I cannot satisfy the one, I will indulge the other.",
    "author": "Mary Shelley"
  },
  {
    "quote": "To talk about oneself a great deal can also be a means of concealing oneself.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "Can you remember who you were, before the world told you who you should be?",
    "author": "Charles Bukowski"
  },
  {
    "quote": "I do not miss childhood, but I miss the way I took pleasure in small things, even as greater things crumbled. I could not control the world I was in, could not walk away from things or people or moments that hurt, but I took joy in the things that made me happy.",
    "author": "Neil Gaiman"
  },
  {
    "quote": "At some point, your memories, your stories, your adventures, will be the only things you'll have left.",
    "author": "Chuck Palahniuk"
  },
  {
    "quote": "You can't judge something as factual without presupposing the validity of certain intuitions.",
    "author": "Sam Harris"
  },
  {
    "quote": "My purpose is to explain, not the meaning of words, but the nature of things.",
    "author": "Baruch Spinoza"
  },
  {
    "quote": "Men fear thought as they fear nothing else on earth – more than ruin, more even than death.",
    "author": "Bertrand Russell"
  },
  {
    "quote": "Tactics is knowing what to do when there is something to do; Strategy is knowing what to do when there is nothing to do.",
    "author": "Savielly Tartakower"
  },
  {
    "quote": "Those who lack the courage will always find a philosophy to justify it.",
    "author": "Albert Camus"
  },
  {
    "quote": "An action that can coexist with the autonomy of the will is permitted; one that does not accord with it is forbidden.",
    "author": "Immanuel Kant"
  },
  {
    "quote": "They lived and laughed and loved and left.",
    "author": "James Joyce"
  },
  {
    "quote": "The world is a possibility if only you'll discover it.",
    "author": "Ralph Ellison"
  },
  {
    "quote": "And I urge you to please notice when you are happy, and exclaim or murmur or think at some point, 'If this isn't nice, I don't know what is.'",
    "author": "Kurt Vonnegut"
  },
  {
    "quote": "The purest heart is precisely the one most willing to comprehend his own guilt most deeply.",
    "author": "Søren Kierkegaard"
  },
  {
    "quote": "If you're in pitch blackness, all you can do is sit tight until your eyes get used to the dark.",
    "author": "Haruki Murakami"
  },
  {
    "quote": "Probably human cruelty is fixed and eternal. Only styles change.",
    "author": "Martin Amis"
  },
  {
    "quote": "It is a mark of a trained mind never to expect more precision in the treatment of any subject than the nature of that subject permits.",
    "author": "Aristotle"
  },
  {
    "quote": "He that would make his own liberty secure must guard even his enemy from oppression; for if he violates this duty he establishes a precedent that will reach to himself.",
    "author": "Thomas Paine"
  },
  {
    "quote": "It is not the intensity but the duration of pain that breaks the will to resist.",
    "author": "William S. Burroughs"
  },
  {
    "quote": "Far from idleness being the root of all evil, it is rather the only true good.",
    "author": "Søren Kierkegaard"
  },
  {
    "quote": "Beware those who seek constant crowds, for they are nothing alone.",
    "author": "Charles Bukowski"
  },
  {
    "quote": "Ideas are not limited by budget. The creative process takes place in your head.",
    "author": "Hans Zimmer"
  },
  {
    "quote": "Always recognize that human individuals are ends, and do not use them as means to your end.",
    "author": "Karl Popper"
  },
  {
    "quote": "All intelligent thoughts have already been thought; what is necessary is only to try to think them again.",
    "author": "Johann Wolfgang von Goethe"
  },
  {
    "quote": "Better to reign in Hell, than serve in Heav'n.",
    "author": "John Milton"
  },
  {
    "quote": "The most dangerous follower is he whose defection would destroy the whole party: that is to say, the best follower.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "Many a man fails to become a thinker only because his memory is too good.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "The remedy of force can never supply the remedy of reason.",
    "author": "Thomas Paine"
  },
  {
    "quote": "Truly amazing, what people can get used to, as long as there are a few compensations.",
    "author": "Margaret Atwood"
  },
  {
    "quote": "Our culture suffers from a hyper-masculine inflation that has us alienated, imagining ourselves to be separate from each other and the earth.",
    "author": "Gary S. Bobroff"
  },
  {
    "quote": "The balance of power between the citizenry and the government is becoming that of the ruling and the ruled, as opposed to actually the elected and the electorate.",
    "author": "Edward Snowden"
  },
  {
    "quote": "There is one thing stronger than all the armies of the world, and that is an idea whose time has come.",
    "author": "Victor Hugo"
  },
  {
    "quote": "Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say.",
    "author": "Edward Snowden"
  },
  {
    "quote": "There is surely nothing quite so useless as doing with great efficiency what should not be done at all.",
    "author": "Peter Drucker"
  },
  {
    "quote": "We have to stop consuming our culture, we have to create culture.",
    "author": "Terence McKenna"
  },
  {
    "quote": "For every thousand hacking at the leaves of evil, there is one striking at the root.",
    "author": "Henry Thoreau"
  },
  {
    "quote": "Nature to be commanded, must be obeyed.",
    "author": "Francis Bacon"
  },
  {
    "quote": "When the people allow a government to dictate the foods they put in their mouth, and the medicines they take into their bodies, their souls will soon be in the same sorry state as those who are ruled by tyranny.",
    "author": "Thomas Jefferson"
  },
  {
    "quote": "There are two ways to be fooled. One is to believe what isn't true; the other is to refuse to believe what's true.",
    "author": "Søren Kierkegaard"
  },
  {
    "quote": "If we do nothing, new technologies will give the government new automatic surveillance capabilities that Stalin could never have dreamed of. The only way to hold the line on privacy in the information age is strong cryptography.",
    "author": "Phil Zimmerman"
  },
  {
    "quote": "Hold yourself responsible for a higher standard than anybody expects of you. Never excuse yourself.",
    "author": "Henry Ward Beecher"
  },
  {
    "quote": "The purpose of education is not to validate ignorance but to overcome it.",
    "author": "Lawrence Krauss"
  },
  {
    "quote": "From my weakness, I drew strength that never left me.",
    "author": "Jorge Luis Borges"
  },
  {
    "quote": "There are years that ask questions and years that answer.",
    "author": "Zora Neale Hurston"
  },
  {
    "quote": "What's done to children, they will do to society.",
    "author": "Karl A. Menninger"
  },
  {
    "quote": "The most beautiful thing we can experience is the mysterious. It is the source of all true art and science.",
    "author": "Albert Einstein"
  },
  {
    "quote": "Poetry is a naked woman, a naked man, and the distance between them.",
    "author": "Lawrence Ferlinghetti"
  },
  {
    "quote": "If fear is cultivated it will become stronger, if faith is cultivated it will achieve mastery.",
    "author": "John Paul Jones"
  },
  {
    "quote": "To get the full value of a joy you must have somebody to divide it with.",
    "author": "Mark Twain"
  },
  {
    "quote": "Find ecstasy in life; the mere sense of living is joy enough.",
    "author": "Emily Dickinson"
  },
  {
    "quote": "Joy can be real only if people look upon their life as a service, and have a definite object in life outside themselves and their personal happiness.",
    "author": "Leo Tolstoy"
  },
  {
    "quote": "When you do things from your soul, you feel a river moving in you, a joy.",
    "author": "Jalāl Rumi"
  },
  {
    "quote": "There is no greater joy nor greater reward than to make a fundamental difference in someone's life.",
    "author": "Mary Rose McGeady"
  },
  {
    "quote": "It is the duty of righteous men to make war on all undeserved privilege, but one must not forget that this is a war without end.",
    "author": "Primo Levi"
  },
  {
    "quote": "Others have seen what is and asked why. I have seen what could be and asked why not.",
    "author": "Pablo Picasso"
  },
  {
    "quote": "The two most important days in your life are the day you are born ... and the day you find out why.",
    "author": "Mark Twain"
  },
  {
    "quote": "Don't be afraid of death; be afraid of an unlived life. You don't have to live forever, you just have to live. ",
    "author": "Natalie Babbitt"
  },
  {
    "quote": "A great deal of intelligence can be invested in ignorance when the need for illusion is deep.",
    "author": "Saul Bellow"
  },
  {
    "quote": "For human nature is such that sorrows and sufferings simultaneously endured do not add up to a whole in our consciousness but hide, the lesser behind the greater, according to a definite law of perspective.",
    "author": "Primo Levi"
  },
  {
    "quote": "The surface of the Earth is the shore of the cosmic ocean.",
    "author": "Carl Sagan"
  },
  {
    "quote": "The creations of our mind should be a blessing, not a curse to mankind.",
    "author": "Albert Einstein"
  },
  {
    "quote": "Where they have burned books they will end in burning men.",
    "author": "Heinrich Heine"
  },
  {
    "quote": "Justice does not require that men must stand idly by while others destroy the basis of their existence.",
    "author": "John Rawls"
  },
  {
    "quote": "Man is sometimes extraordinarily, passionately, in love with suffering.",
    "author": "Fyodor Dostoyevsky"
  },
  {
    "quote": "Trying to define yourself is like trying to bite your own teeth.",
    "author": "Alan Watts"
  },
  {
    "quote": "Since Auschwitz we know what man is capable of. And since Hiroshima we know what is at stake.",
    "author": "Viktor Frankl"
  },
  {
    "quote": "Thinking is the enemy of creativity.",
    "author": "Ray Bradbury"
  },
  {
    "quote": "Religion is regarded by the common people as true, by the wise as false, and by the rulers as useful.",
    "author": "Seneca"
  },
  {
    "quote": "You always admire what you really don't understand.",
    "author": "Blaise Pascal"
  },
  {
    "quote": "Men can acquire knowledge, but not wisdom. Some of the greatest fools ever known were learned men.",
    "author": "Proverbs"
  },
  {
    "quote": "The young man knows the rules, but the old man knows the exceptions.",
    "author": "Oliver Wendell Holmes"
  },
  {
    "quote": "There are more mysteries contained in the shadow of a person who walks in the sunlight than in all the religions of mankind; past, present, and future.",
    "author": "Giorgio de Chirico"
  },
  {
    "quote": "The highest result of education is tolerance.",
    "author": "Helen Keller"
  },
  {
    "quote": "There's a natural mystic blowing through the air. If you listen carefully now, you will hear.",
    "author": "Bob Marley"
  },
  {
    "quote": "The mind commands the body and immediately it obeys. The mind orders itself, and meets resistance.",
    "author": "St. Augustine"
  },
  {
    "quote": "Beggars do not envy millionaires, though of course they will envy other beggars who are more successful.",
    "author": "Bertrand Russell"
  },
  {
    "quote": "Associate with people who are likely to improve you.",
    "author": "Seneca"
  },
  {
    "quote": "Words that come from the heart are always simple.",
    "author": "Albert Camus"
  },
  {
    "quote": "The greatest danger for most of us is not that our aim is too high and we miss it, but that it is too low and we reach it.",
    "author": "Michelangelo"
  },
  {
    "quote": "It is not necessary that whilst I live I live happily; but it is necessary that so long as I live I should live honourably.",
    "author": "Immanuel Kant"
  },
  {
    "quote": "I have always observed that to succeed in the world one should appear like a fool but be wise.",
    "author": "Charles de Secondat"
  },
  {
    "quote": "At the crucial moment, I conquered myself. I remained alive. But I want you to know one thing: it takes courage to choose life under such circumstances.",
    "author": "Henrik Ibsen"
  },
  {
    "quote": "Hate what the world seeks, and seek what it avoids.",
    "author": "St. Ignatius of Loyola"
  },
  {
    "quote": "Watching television is like taking black spray paint to your third eye.",
    "author": "Bill Hicks"
  },
  {
    "quote": "There is no creation without tradition, the 'new' is an inflection on a preceding form; novelty is always a variation on the past.",
    "author": "Carlos Fuentes"
  },
  {
    "quote": "I must change my life so that I can live it, not wait for it.",
    "author": "Susan Sontag"
  },
  {
    "quote": "You can never be overdressed or overeducated.",
    "author": "Oscar Wilde"
  },
  {
    "quote": "Test yourself on mankind. It is something that makes the doubter doubt, the believer believe.",
    "author": "Franz Kafka"
  },
  {
    "quote": "You never know how strong you are until being strong is your only choice.",
    "author": "Bob Marley"
  },
  {
    "quote": "Without deviation from the norm, progress is not possible.",
    "author": "Frank Zappa"
  },
  {
    "quote": "Only in books do we learn what's really going on.",
    "author": "Kurt Vonnegut"
  },
  {
    "quote": "Between the wish and the thing the world lies waiting.",
    "author": "Cormac McCarthy"
  },
  {
    "quote": "A truly good book teaches me better than to read it. I must soon lay it down, and commence living on its hint. What I began by reading, I must finish by acting.",
    "author": "Henry David Thoreau"
  },
  {
    "quote": "I had never feared insomnia before — like prison, wouldn't it just give you more time to read?",
    "author": "Lorrie Moore"
  },
  {
    "quote": "Mirrors and images. Or sex and love. These are two separate systems that we miserably try to link.",
    "author": "Don DeLillo"
  },
  {
    "quote": "Let us beware of saying that death is the opposite of life. The living being is only a species of the dead, and a very rare species.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "Political questions are far too serious to be left to the politicians.",
    "author": "Hannah Arendt"
  },
  {
    "quote": "You never know what worse luck your bad luck has saved you from.",
    "author": "Cormac McCarthy"
  },
  {
    "quote": "Only the spontaneous action of the people themselves can create liberty.",
    "author": "Mikhail Bakunin"
  },
  {
    "quote": "Life is very short and anxious for those who forget the past, neglect the present, and fear the future.",
    "author": "Seneca"
  },
  {
    "quote": "Reality is a crutch for people who can't handle drugs.",
    "author": "Lily Tomlin"
  },
  {
    "quote": "Your naked body should only belong to those who fall in love with your naked soul.",
    "author": "Charlie Chaplin"
  },
  {
    "quote": "Without suffering, there'd be no compassion.",
    "author": "Nicholas Sparks"
  },
  {
    "quote": "To one man, solitude is the escape of an invalid; for another, it is escape from the invalids.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "I believe there is another world waiting for us. A better world. And I'll be waiting for you there.",
    "author": "David Mitchell"
  },
  {
    "quote": "The cradle rocks above an abyss, and common sense tells us that our existence is but a brief crack of light between two eternities of darkness.",
    "author": "Vladimir Nabokov"
  },
  {
    "quote": "Nowhere can man find a quieter or more untroubled retreat than in his own soul.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "Every man is born as many men and dies as a single one.",
    "author": "Martin Heidegger"
  },
  {
    "quote": "The ascent of the individual is punctuated by periods of dissolution and rebirth.",
    "author": "Jordan Peterson"
  },
  {
    "quote": "Do not go gentle into that good night. Rage, rage against the dying of the light.",
    "author": "Dylan Thomas"
  },
  {
    "quote": "When I let go of what I am, I become what I might be.",
    "author": "Lao Tzu"
  },
  {
    "quote": "Don't be afraid your life will end. Be afraid that it will never begin.",
    "author": "Grace Hansen"
  },
  {
    "quote": "Freedom is the freedom to say that two plus two make four. If that is granted, all else follows.",
    "author": "George Orwell"
  },
  {
    "quote": "As soon as you trust yourself, you will know how to live.",
    "author": "Johann Wolfgang von Goethe"
  },
  {
    "quote": "To be rooted is perhaps the most important and least recognized need of the human soul.",
    "author": "Simone Weil"
  },
  {
    "quote": "Action is limited and relative. Unlimited and absolute is the vision of him who sits at ease and watches, who walks in loneliness and dreams.",
    "author": "Oscar Wilde"
  },
  {
    "quote": "My soul's a burden to me, I've had enough of it. I'm eager to be in that country, where the sun kills every question. I don't belong here.",
    "author": "Albert Camus"
  },
  {
    "quote": "Just remember, when you think all is lost, the future remains.",
    "author": "Dr. Robert H. Goddard"
  },
  {
    "quote": "It takes a great deal of courage to see the world in all its tainted glory and still to love it.",
    "author": "Oscar Wilde"
  },
  {
    "quote": "When people complain of life, it is almost always because they have asked impossible things of it.",
    "author": "Ernest Renan"
  },
  {
    "quote": "In a world of diminishing mystery, the unknown persists.",
    "author": "Jhumpa Lahiri"
  },
  {
    "quote": "Never apologize for showing your feelings. When you do, you are apologizing for the truth.",
    "author": "José N. Harris"
  },
  {
    "quote": "When you arise in the morning think of what a privilege it is to be alive, to think, to enjoy, to love.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "The less you say, the more weight your words will carry.",
    "author": "Leigh Bardugo"
  },
  {
    "quote": "Fire tries gold, misfortune tries brave men.",
    "author": "Seneca"
  },
  {
    "quote": "The wise person esteems everyone, for he recognizes the good in each, and he realizes how hard it is to do things well.",
    "author": "Baltasar Gracián"
  },
  {
    "quote": "Even the most courageous among us only rarely has the courage to face what he already knows.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "If there must be trouble, let it be in my day, that my child may have peace.",
    "author": "Thomas Paine"
  },
  {
    "quote": "It is our light, not our darkness that most frightens us.",
    "author": "Marianne Williamson"
  },
  {
    "quote": "No trumpets sound when the important decisions of our life are made. Destiny is made known silently.",
    "author": "Agnes De Mille"
  },
  {
    "quote": "People seem not to see that their opinion of the world is also a confession of character.",
    "author": "Ralph Waldo Emerson"
  },
  {
    "quote": "A quiet and modest life brings more joy than a pursuit of success bound with constant unrest.",
    "author": "Albert Einstein"
  },
  {
    "quote": "God knows everything; except where God came from. God is experiencing consciousness through all of us.",
    "author": "Alex Jones"
  },
  {
    "quote": "If you are under the impression you have already perfected yourself, you will never rise to the heights you are no doubt capable of.",
    "author": "Kazuo Ishiguro"
  },
  {
    "quote": "The rich man is always sold to the institution which makes him rich.",
    "author": "Henry David Thoreau"
  },
  {
    "quote": "To become what one is, one must not have the faintest idea what one is.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "Time is what we want most,but what we use worst.",
    "author": "William Penn"
  },
  {
    "quote": "Death is not the greatest loss in life. The greatest loss is what dies inside us while we live.",
    "author": "Norman Cousins"
  },
  {
    "quote": "Poison is in everything, and no thing is without poison. The dosage makes it either a poison or a remedy.",
    "author": "Paracelsus"
  },
  {
    "quote": "None are more hopelessly enslaved then those who falsely believe they are free.",
    "author": "Goethe"
  },
  {
    "quote": "If you are the follower of an ideological system, you have avoided the responsibility of thinking for yourself.",
    "author": "Jordan B. Peterson"
  },
  {
    "quote": "We're separated from the infinite by death and ignorance. We don't know. We contend, we wrestle.",
    "author": "Jordan B. Peterson"
  },
  {
    "quote": "No, a fool learns from experience. A wise man learns from the experience of others.",
    "author": "Otto Von Bismarck"
  },
  {
    "quote": "You will not be punished for your anger, you will be punished by your anger.",
    "author": "Buddha"
  },
  {
    "quote": "The more a thing tends to be permanent, the more it tends to be lifeless.",
    "author": "Alan Watts"
  },
  {
    "quote": "Fairy tales are more than true: not because they tell us that dragons exist, but because they tell us that dragons can be beaten.",
    "author": "Neil Gaiman"
  },
  {
    "quote": "I sympathize with everybody, damn it, and see why they are the way they are.",
    "author": "Kurt Vonnegut"
  },
  {
    "quote": "Dare to think for yourself.",
    "author": "Voltaire"
  },
  {
    "quote": "But in the end one needs more courage to live than to kill himself.",
    "author": "Albert Camus"
  },
  {
    "quote": "You have enemies? Good, that means you've stood up for something, sometime in your life.",
    "author": "Winston Churchill"
  },
  {
    "quote": "The reason I talk to myself is because I'm the one who's answers I accept.",
    "author": "George Carlin"
  },
  {
    "quote": "We can destroy what we have written, but we cannot unwrite it.",
    "author": "Anthony Burgess"
  },
  {
    "quote": "What is done in love is done well.",
    "author": "Vincent van Gogh"
  },
  {
    "quote": "Think of yourself as dead. You have lived your life. Now, take what's left and live it properly.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "To imagine — to dream about things that have not happened — is among mankind's deepest needs.",
    "author": "Milan Kundera"
  },
  {
    "quote": "The wise man sees in the misfortune of others what he should avoid.",
    "author": "Marcus Aurelius"
  },
  {
    "quote": "Always, sooner or later, the noble cause triumphs.",
    "author": "Pablo Neruda"
  },
  {
    "quote": "Birthdays cause a man to reflect on what little he has accomplished.",
    "author": "Albert Einstein."
  },
  {
    "quote": "But no price is too high to pay for the privilege of owning yourself.",
    "author": "Friedrich Nietzsche"
  },
  {
    "quote": "When a person can't find a deep sense of meaning, they distract themselves with pleasure.",
    "author": "Viktor Frankl"
  },
  {
    "quote": "Learn from the mistakes of others. You can't live long enough to make them all yourselves!",
    "author": "Chanakya"
  },
  {
    "quote": "History is the nightmare from which I am trying to awake.",
    "author": "James Joyce"
  },
  {
    "quote": "Learn to value yourself, which means: fight for your happiness.",
    "author": "Ayn Rand "
  },
  {
    "quote": "Doubt is not a pleasant condition, but certainty is absurd.",
    "author": "Voltaire  "
  },
  {
    "quote": "May my heart be kind, my mind fierce, and my spirit brave.",
    "author": "Kate Forsyth"
  },
  {
    "quote": "Three o'clock is always too late or too early for anything you want to do.",
    "author": "Jean-Paul Sartre"
  },
  {
    "quote": "The rage for revenge is hard to exorcise.",
    "author": "Derek Walcott"
  },
  {
    "quote": "Enlightenment is when a wave realizes it is the ocean.",
    "author": "Thich Nhat Hanh"
  },
  {
    "quote": "I'm a member of no party. I have no ideology. I'm a rationalist; I do what I can in the international struggle between science and reason, and the barbarism, superstition and stupidity that's all around us.",
    "author": "Christopher Hitchens"
  },
  {
    "quote": "How we spend our days is, of course, how we spend our lives. What we do with this hour, and that one, is what we are doing.",
    "author": "Annie Dillard"
  },
  {
    "quote": "The hardest thing in the world to understand is the income tax.",
    "author": "Albert Einstein"
  },
  {
    "quote": "The idealism of every era is the cover story of it's greatest thefts.",
    "author": "Eric Weinstein"
  },
  {
    "quote": "Information is power, but like all power, there are those who want to keep it for themselves.",
    "author": "Aaron Swartz"
  },
  {
    "quote": "Don't get stuck in the past when there is a whole future ahead of you.",
    "author": "Roberto Font"
  },
  {
    "quote": "Never be a spectator of unfairness or stupidity. The grave will supply plenty of time for silence.",
    "author": "Christopher Hitchens"
  },
  {
    "quote": "How we spend our days is, of course, how we spend our lives. What we do with this hour, and that one, is what we are doing.",
    "author": "Annie Dillard"
  },
  {
    "quote": "The art of writing is the art of discovering what you believe.",
    "author": "Gustave Flaubert"
  },
  {
    "quote": "Prosperity tries the fortunate, adversity the great.",
    "author": "Rose Kennedy"
  },
  {
    "quote": "The first half of life is devoted to forming a healthy ego, the second half is going inward and letting go of it.",
    "author": "Carl Jung"
  },
  {
    "quote": "One death is a tragedy; a million is a statistic.",
    "author": "Joseph Stalin"
  },
  {
    "quote": "What is hardest to accept about the passage of time is that the people who once mattered the most to us wind up in parentheses.",
    "author": "John Irving"
  },
  {
    "quote": "No tree can grow to heaven unless it's roots reach down to hell.",
    "author": "Carl Jung"
  },
  {
    "quote": "We make a living by what we get, but we make a life by what we give.",
    "author": "Winston Churhhill"
  },
  {
    "quote": "Ideology always paves the way toward atrocity.",
    "author": "Terence McKenna"
  },
  {
    "quote": "You have to suffer in order to be a human being who can help people understand suffering.",
    "author": "Nicholson Baker"
  },
  {
    "quote": "As you think, so shall you become.",
    "author": "Bruce Lee"
  },
  {
    "quote": "Indifference and neglect often do much more damage than outright dislike.",
    "author": "Albus Dumbledore"
  },
  {
    "quote": "Life is so beautiful that death has fallen in love with it, a jealous, possessive love that grabs at what it can.",
    "author": "Yann Martel"
  },
  {
    "quote": "Once, I saw a bee drown in honey, and I understood.",
    "author": "Nikos Kazantzakis"
  },
  {
    "quote": "The more fundamental the axiom you're assaulting, the more dangerous is your assault.",
    "author": "Jordan B Peterson"
  },
  {
    "quote": "Courage is not the absence of fear but acting in spite of it.",
    "author": "General Donald Doyle"
  },
  {
    "quote": "On the throne of the world, any delusion can become fact.",
    "author": "Gore Vidal"
  },
  {
    "quote": "If everyone is thinking alike, then somebody isn't thinking.",
    "author": "General George Patton"
  },
  {
    "quote": "Beware of those quick to praise, for they need praise in return.",
    "author": "Charles Bukowski"
  },
  {
    "quote": "Yesterday I was clever, so I wanted to change the world. Today I am wise, so I am changing myself.",
    "author": "Jalal Rumi"
  },
  {
    "quote": "It is not that I'm so smart. But I stay with the questions much longer.",
    "author": "Albert Einstein"
  },
  {
    "quote": "The question isn't who is going to let me; it's who is going to stop me.",
    "author": "Ayn Rand"
  },
  {
    "quote": "A good book, he had concluded, leaves you wanting to reread the book. A great book compels you to reread your own soul.",
    "author": "Richard Flanagan"
  },
  {
    "quote": "Most men lead lives of quiet desperation and go to their graves with the song still in them.",
    "author": "Henry David Thoreau"
  },
  {
    "quote": "The antidote for fifty enemies is one friend.",
    "author": "Aristotle"
  },
  {
    "quote": "The man who writes about himself and his own time is the only man who writes about all people and all time.",
    "author": "George Bernard Shaw"
  },
  {
    "quote": "The greatest prison we live in, is the fear what other people think.",
    "author": "David Icke"
  },
  {
    "quote": "When nobody wakes you up in the morning, and when nobody waits for you at night, what do you call it, freedom or loneliness?",
    "author": "Charles Bukowski"
  },
  {
    "quote": "The lifelong friends… we sometimes wait a lifetime for them.",
    "author": "Ali Smith"
  },
  {
    "quote": "Never forget that you have every right to question any individual, system, movement, or group that only tolerates you when you think and behave exactly like them.",
    "author": "Africa Brooke"
  },
  {
    "quote": "Envy is thin because it bites but never eats.",
    "author": "Spanish Proverb"
  },
  {
    "quote": "Nobody realizes that some people expend their tremendous energy merely to be normal.",
    "author": "Albert Camus"
  },
  {
    "quote": "A lie is more comfortable than doubt, more useful than love, more lasting than truth.",
    "author": "Gabriel García Márquez"
  },
  {
    "quote": "The fact that a symphony ends doesn't mean that it's not worth listening to.",
    "author": "Jordan B Peterson"
  },
  {
    "quote": "There's a tremendous power in using the least amount of information to get a point across.",
    "author": "Rick Rubin"
  },
  {
    "quote": "I underestimate myself, and that itself means an overestimation of others.",
    "author": "Franz Kafka"
  },
  {
    "quote": "What matters in life is not what happens to you but what you remember and how you remember it.",
    "author": "Gabriel Garcia Marquez"
  },
  {
    "quote": "Not all those who wander are lost.",
    "author": "J. R. R. Tolkien"
  }
];

_QUOTE_DB._COUNT = _QUOTE_DB._LIST.length;