function generateRandomIndex(maxCount) {
  return ~~(Math.random() * (maxCount - 1));
}

function setContentByIdentifier(domNodeIdentifier, contentText) {
  const element = document.querySelector(domNodeIdentifier);
  if (element === null || element === undefined) {
    console.warn('Invalid identifier provided: ' + domNodeIdentifier + ' , no element found');
    return;
  }
  element.innerText = contentText;
}

function main() {
  // generate a random quote number
  const QuoteTextId = '#quote-text';
  const QuoteAuthorId = '#quote-author';
  const quoteIndex = generateRandomIndex(_QUOTE_DB._COUNT);
  const quoteContent = _QUOTE_DB._LIST[quoteIndex];
  // inject the contents
  setContentByIdentifier(QuoteTextId, quoteContent.quote);
  setContentByIdentifier(QuoteAuthorId, quoteContent.author);
  // all done, perform cleanup
  _QUOTE_DB._LIST.length = 0;
  document.removeEventListener('DOMContentLoaded', main);
}

document.addEventListener('DOMContentLoaded', main);