# QuoteTab

![](https://gitlab.com/ashraful/firefox-extension-quote-tab/-/raw/master/preview.png)

# About

A simple firefox extension to display random quotes on new-tab page.
Usually, new tab pages are bloated with news, search, promoted contents making the page un-interesting.
QuoteTab replaces the new-tab page with a simple page containing a historical(or fictional) quote with author name.
Every new tab is a new quote of interest, something to make your day. :)

# Requirements

Currently, the extension works on Firefox 85.0 or higher.

# Attributions

- The beautiful icon was collected from [Flaticon](https://flaticon.com) contributed by [Freepik](https://www.freepik.com)
- The quotes collection was copied from [Quollated.com](https://quollated.com/) created by wonderful [Paramdeo Singh](https://paramdeo.com)

# License

[MIT](https://mit-license.org/)